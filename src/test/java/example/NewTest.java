package example;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;

public class NewTest {
	public WebDriver driver; 
	@BeforeTest
	public void beforeTest() {	
		System.setProperty("webdriver.gecko.driver","src/test/resources/geckodriver.exe");
	    driver = new FirefoxDriver();  
	}	
	
	@Test				
	public void testEasy() {	
		driver.get("http://demo.guru99.com/test/guru99home/");  
		String title = driver.getTitle();				 
		Assert.assertTrue(title.contains("Demo Guru99 Page")); 		
	}	
		
	@AfterTest
	public void afterTest() {
		driver.quit();			
	}		

}
